package conduit.api.scenarios;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import conduit.api.specs.UserSpecification;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import net.serenitybdd.junit5.SerenityJUnit5Extension;

@TestMethodOrder(MethodOrderer.MethodName.class)
@ExtendWith(SerenityJUnit5Extension.class)
public class UserTests {
    
    @Steps
    UserSpecification user;

    @Test
    @Title("1.01: Deve autenticar com sucesso ao informar email e senha válidos")
    public void t101() {
        user.loginWithEmailAndPassword("matheus.kerr@bestcode.com.br", "best123");
        user.checkStatusCodeIs(201);
        user.checkSuccessfulLogin(true);
        user.checkMessageContainsText("Logged in successfully");
        user.checkSessionUsernameIs("mkerr");
        user.checkSessionTokenIsValid();
    }

    @ParameterizedTest
    @CsvSource(textBlock = """
        matheus.kerr@bestcode.com.br | senha incorreta  | Invalid account
        ''                           | senha            | property email has failed the following constraints: isEmail
        matheus.kerr@bestcode.com.br | ''               | property password has failed the following constraints: isNotEmpty
        """, delimiterString = "|"
    )
    @Title("1.02: Não deve autenticar ao informar combinação de email e senha incorreta")
    public void t102(String email, String senha, String mensagem) {
        user.loginWithEmailAndPassword(email, senha);
        user.checkStatusCodeIs(400);
        user.checkSuccessfulLogin(false);
        user.checkMessageContainsText(mensagem);
    }

}
