package conduit.api.specs;

import static net.serenitybdd.rest.SerenityRest.*;
import static org.hamcrest.CoreMatchers.*;
import io.restassured.RestAssured;
import io.restassured.config.ObjectMapperConfig;
import io.restassured.mapper.ObjectMapperType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import net.thucydides.core.annotations.Step;

public class CommonSpecification {

    RequestSpecification request;
    Response response;

    public CommonSpecification() {
        RestAssured.baseURI = "http://localhost/api";
        RestAssured.port = 3333;
        request =        
            given()
            .config(RestAssured.config()
            .objectMapperConfig(new ObjectMapperConfig(ObjectMapperType.GSON)))
            //.log().all()
            .contentType("application/json");
    }

    @Step
    public void checkStatusCodeIs(int statusCode) {
        response.then()
            .statusCode(equalTo(statusCode));
    }

    @Step
    public void checkMessageContainsText(String messageString) {
        response.then()
            .body("message", containsString(messageString));
    }

}
