package conduit.api.specs;

import com.google.gson.JsonObject;
import static org.hamcrest.text.CharSequenceLength.*;
import org.hamcrest.Matchers;
import net.thucydides.core.annotations.Step;
import static org.hamcrest.CoreMatchers.*;

public class UserSpecification extends CommonSpecification{
    
    @Step
    public void loginWithEmailAndPassword(String email, String password) {
        JsonObject json = new JsonObject();
        json.addProperty("email", email);
        json.addProperty("password", password);
        response = request.when()
            .body(json)
            .post("/users/login");
    }

    @Step
    public void checkSuccessfulLogin(boolean success) {
        response.then()
            .body("success", is(success));
    }

    @Step
    public void checkSessionUsernameIs(String username) {
        response.then()
            .body("data.username", equalTo(username));
    }

    @Step
    public void checkSessionTokenIsValid() {
        response.then()
            .body("data.token", startsWith("ey"))
            .and()
            .body("data.token", hasLength(Matchers.greaterThan(30)));
    }    

}
